import { Component, Input } from '@angular/core';
import { Video } from '../video.interface';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent {

  @Input() videoData: Video;

}
