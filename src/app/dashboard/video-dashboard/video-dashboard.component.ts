import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Video } from '../video.interface';
import { VideoService } from '../video.service';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent implements OnInit {

  myVideoList: Observable<Video[]>;

  selectedVideo: Video;

  constructor( private videoService: VideoService) {
  }

  ngOnInit() {
    this.myVideoList = this.videoService.getVideos();
  }

  playVideo( video: Video ) {
    this.selectedVideo = video;
  }

}
