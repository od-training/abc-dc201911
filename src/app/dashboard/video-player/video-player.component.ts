import { Component, OnInit, Input } from '@angular/core';
import { Video } from '../video.interface';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent implements OnInit {
  @Input() videoData: Video | undefined;

  constructor() { }

  ngOnInit() {
  }

}
