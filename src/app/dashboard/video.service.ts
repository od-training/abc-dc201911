import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Video } from './video.interface';

@Injectable({
  providedIn: 'root'
})
export class VideoService {
  private videoUrl = 'https://api.angularbootcamp.com/videos';

  constructor(private http: HttpClient) {}

  getVideos(): Observable<Video[]> {
    return this.http.get<Video[]>(this.videoUrl).pipe(
      map(upperCaseTitles)
    );
  }
}

function upperCaseTitles(videos: Video[]): Video[] {
  return videos.map(v => {
    v.title = v.title.toUpperCase();
    return v;
  });
}
