export interface Video {
  title: string;
  author: string;
  id: string;
  viewDetails: VideoDetails[];
}

export interface VideoDetails {
  age: number;
  region: string;
  date: string;
}
