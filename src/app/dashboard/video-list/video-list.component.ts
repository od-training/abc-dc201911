import {Component, Input, Output, EventEmitter} from '@angular/core';
import { Video } from '../video.interface';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent {
  @Input() videoList;

  @Output() videoSelected = new EventEmitter<Video>();

  selectedVideo;

  selectVideo( video ) {
    this.selectedVideo = video;
    this.videoSelected.emit(video);
  }
}
